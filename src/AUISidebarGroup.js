import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import AUINav from './AUINav';
import AUINavHeading from './AUINavHeading';

const AUISidebarGroup = ({ groupId, tier, heading, options, children, className }) => {
    const classes = classnames('aui-sidebar-group', `aui-sidebar-group-tier-${tier}`, groupId, className);

    const header = (typeof heading === 'string') ? <AUINavHeading>{heading}</AUINavHeading> : heading;

    return (
        <div id={groupId} style={{paddingTop:15}} className={classes}>
            <div className="aui-navgroup-inner">
                { header }

                <AUINav options={options}>{children}</AUINav>
            </div>
        </div>
    );
};

AUISidebarGroup.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    options: PropTypes.array,
    groupId: PropTypes.string.isRequired,
    heading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]).isRequired,
    tier: PropTypes.string
};

AUISidebarGroup.defaultProps = {
    tier: 'one'
};

export default AUISidebarGroup;
