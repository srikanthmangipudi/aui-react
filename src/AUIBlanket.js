import React from 'react';
import PropTypes from 'prop-types';

const AUIBlanket = ({ style, onClick }) => (
    <div
        className="aui-blanket"
        tabIndex="0"
        style={{zIndex: 2980, ...style}}
        aria-hidden={false}
        onClick={onClick}
    ></div>
);

AUIBlanket.propTypes = {
    /**
     * Custom style for the mask
     */
    style: PropTypes.object,
    /**
     * Click event to trigger on an element
     */
    onClick: PropTypes.func
};

export default AUIBlanket;
