import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import AUIIcon from './AUIIcon';

const AUIButton = ({ icon, type, disabled, children, className, ...otherProps }) => {
    let iconComponent = null;

    if (icon) {
        iconComponent = <span><AUIIcon icon={icon}/>&nbsp;</span>;
    }

    let typeClass;

    if (type) {
        typeClass = `aui-button-${type}`;
    }

    const classes = classnames('aui-button', typeClass, className);

    return (
        <button className={classes} aria-disabled={disabled} {...otherProps}>{iconComponent}{children}</button>
    );
};

AUIButton.defaultProps = {
    className: ''
};

AUIButton.propTypes = {
    /**
     * The type of the button link, primary or subtle
     */
    type: PropTypes.oneOf(['link', 'primary', 'subtle']),
    /**
     * The icon to show in the button
     */
    icon: PropTypes.string,
    /**
     * Disabled
     */
    disabled: PropTypes.bool,
    /**
     * The children of the button
     */
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
    ]),
    className: PropTypes.string
};

export default AUIButton;
